Name:		ypbind
Epoch: 		3
Version:	2.7.2
Release:        3
Summary:	NIS binding software
License:	GPLv2
URL:		http://www.linux-nis.org/nis/ypbind-mt/index.html
Source0:	https://github.com/thkukuk/ypbind-mt/archive/v%{version}.tar.gz#/ypbind-mt-%{version}.tar.gz
Source1: 	nis.sh
Source2: 	ypbind.service
Source3: 	ypbind-pre-setdomain
Source4: 	ypbind-post-waitbind
Patch0: 	ypbind-1.11-gettextdomain.patch
Patch2: 	ypbind-2.4-gettext_version.patch

BuildRequires:	gcc systemd automake autoconf libxslt
BuildRequires:	dbus-glib-devel systemd-devel docbook-style-xsl
BuildRequires:  libtirpc-devel libnsl2-devel gettext-devel
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Requires: 	rpcbind nss_nis yp-tools >= 4.2.2-2

%description
The Network Information Service (NIS) is a system that provides
network information (login names, passwords, home directories, group
information) to all of the machines on a network. NIS can allow users
to log in on any machine on the network, as long as the machine has
the NIS client programs running and the user's password is recorded in
the NIS passwd database. NIS was formerly known as Sun Yellow Pages
(YP).
 
This package provides the ypbind daemon. The ypbind daemon binds NIS
clients to an NIS domain. Ypbind must be running on any machines
running NIS client programs.
 
Install the ypbind package on any machines running NIS client programs
(included in the yp-tools package). If you need an NIS server, you
also need to install the ypserv package to a machine on your network.

%package	help
Summary: 	Doc files for ypbind
Buildarch:	noarch

%description 	help
The help package contains doc files for ypbind.

%prep
%autosetup -n %{name}-mt-%{version} -p1
autoreconf -fiv

%build
export CFLAGS="$RPM_OPT_FLAGS -fpic"
export LDFLAGS="$LDFLAGS -pie -Wl,-z,relro,-z,now"

%configure
%make_build

%install
%make_install

mkdir -p %{buildroot}/%{_libexecdir}
mkdir -p %{buildroot}/%{_unitdir}
mkdir -p %{buildroot}/%{_sysconfdir}/dhcp/dhclient.d
mkdir -p %{buildroot}/%{_localstatedir}/yp/binding
install -m 755 %{SOURCE1} %{buildroot}/%{_sysconfdir}/dhcp/dhclient.d/nis.sh
install -m 644 %{SOURCE2} %{buildroot}/%{_unitdir}/ypbind.service
install -m 755 %{SOURCE3} %{buildroot}/%{_libexecdir}/ypbind-pre-setdomain
install -m 755 %{SOURCE4} %{buildroot}/%{_libexecdir}/ypbind-post-waitbind
install -m 644 etc/yp.conf %{buildroot}/%{_sysconfdir}/yp.conf
%{find_lang} %{name}

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files -f %{name}.lang
%license COPYING
%config(noreplace) %{_sysconfdir}/yp.conf
%{_sysconfdir}/dhcp/dhclient.d/nis.sh
%{_unitdir}/%{name}.service
%{_sbindir}/%{name}
%{_libexecdir}/%{name}*
%dir %{_localstatedir}/yp/binding
%{_datadir}/locale/*

%files help
%doc README NEWS
%{_mandir}/*/*

%changelog
* Sat Oct 22 2022 gaihuiying <eaglegai@163.com> - 3:2.7.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify description about ypbind

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.7.2-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Jul 28 2020 lunankun <lunankun@huawei.com> - 2.7.2-1
- Type:update
- ID:NA
- SUG:restart
- DESC:update to ypbind-2.7.2

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.6-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Thu Nov 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.6-2
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the release

* Mon Sep 9 2019 luhuaxin <luhuaxin@huawei.com> - 2.6-1
- Package init
